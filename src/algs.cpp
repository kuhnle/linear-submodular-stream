#ifndef ALGS_CPP
#define ALGS_CPP

#include "mygraph.cpp"
#include <set>
#include <map>
#include <string>
#include <vector>
#include <fstream>

using namespace std;
using namespace mygraph;

enum Algs {SG,QS,BS,SS,BR,LG,TG,PP,R,QSALT,CK};

uniform_real_distribution< double > unidist(0, 1);

resultsHandler allResults;
vector< double > alpha; 

void init_alpha( tinyGraph& g ) {
   alpha.assign( g.n, 0.0 );
   mt19937 gen( 0 ); //same sequence each time
   
   for (node_id u = 0; u < g.n; ++u) {
      alpha[u] = unidist( gen );
   }
}

struct Args {
   Algs alg;
   string graphFileName;
   string outputFileName = "";
   size_t k = 2;
   tinyGraph g;
   double tElapsed;
   double wallTime;
   Logger logg;
   bool steal = true;
   double epsi = 0.1;
   double delta = 0.1;
   double c = 1;
   size_t N = 1;
   size_t P = 10;
   bool plusplus = false;
   double tradeoff = 0.5;
   bool quiet = false;
};

class MyPair {
public:
   node_id u;
   double  gain; //may be negative

   MyPair() {}
   MyPair( node_id a,
	   double g ) {
      u = a;
      gain = g;
   }

   MyPair( const MyPair& rhs ) {
      u = rhs.u;
      gain = rhs.gain;
   }

   void operator=( const MyPair& rhs ) {
      u = rhs.u;
      gain = rhs.gain;
   }
};



struct gainLT {
   bool operator() (const MyPair& p1, const MyPair& p2) {
      return p1.gain < p2.gain;
   }
} gainLTobj;

struct revgainLT {
   bool operator() (const MyPair& p1, const MyPair& p2) {
      return (p1.gain > p2.gain);
   }
} revgainLTobj;


vector< bool > emptySetVector;


#ifndef REVMAX
size_t marge( size_t& nEvals, tinyGraph& g, node_id u, vector<bool>& set,
	      vector< bool >& cov = emptySetVector ) {
   
   if (set[u])
      return 0;
   
   ++nEvals;
   
   return g.getDegreeMinusSet( u, cov ) + 1;
}

size_t compute_valSet( size_t& nEvals, tinyGraph& g, vector<bool>& set,
		       vector< bool >& cov = emptySetVector ) {
   ++nEvals;
   cov.assign( g.n, false );
   size_t val = 0;
   for (node_id u = 0 ; u < g.n; ++u) {
      if (set[u]) {
	 if (!cov[u]) {
	    cov[ u ] = true;
	    val += 1;
	 }
	 vector< tinyEdge >& neis = g.adjList[u].neis;
	 for (size_t j = 0; j < neis.size(); ++j) {
	    node_id v = neis[j].target;
	    if (!cov[ v ]) {
	       cov[ v ] = true;
	       val += 1;
	    }
	 }
      }
   }

   return val;
}

size_t compute_valSet( size_t& nEvals, tinyGraph& g, vector<node_id>& set ) {
   ++nEvals;
   vector< bool > cov( g.n, false );

   size_t val = 0;
   for (size_t i = 0; i < set.size(); ++i) {
      node_id u = set[i];
      if (!cov[u]) {
	 cov[u] = true;
	 val += 1;
      }
      vector< tinyEdge >& neis = g.adjList[u].neis;
      for (size_t j = 0; j < neis.size(); ++j) {
	 node_id v = neis[j].target;
	 if (!cov[ v ]) {
	    cov[ v ] = true;
	    val += 1;
	 }
      }
   }

   return val;
}
#else
double compute_valSet( size_t& nEvals, tinyGraph& g, vector<bool>& set,
		       vector< bool >& cov = emptySetVector ) {
   if (alpha.size() == 0) {
      init_alpha( g );
   }
   
   ++nEvals;
   cov.assign( g.n, false );
   double val = 0;
   
   for (node_id u = 0 ; u < g.n; ++u) {
      vector< tinyEdge >& neis = g.adjList[u].neis;
      double valU = 0.0;
      for (size_t j = 0; j < neis.size(); ++j) {
	 node_id v = neis[j].target;
	 if (set[v]) {
	    valU += neis[j].weight;
	 }
      }
      valU = pow( valU, alpha[u] );
      val += valU;
   }

   return val;
}

double compute_valSet( size_t& nEvals, tinyGraph& g, vector<node_id>& sset ) {
   if (alpha.size() == 0) {
      init_alpha( g );
   }
   
   vector< bool > set(g.n, false);
   for (size_t i = 0; i < sset.size(); ++i) {
      set[ sset[i] ] = true;
   }
   
   ++nEvals;

   double val = 0;
   
   for (node_id u = 0 ; u < g.n; ++u) {
      vector< tinyEdge >& neis = g.adjList[u].neis;
      double valU = 0.0;
      for (size_t j = 0; j < neis.size(); ++j) {
	 node_id v = neis[j].target;
	 if (set[v]) {
	    valU += neis[j].weight;
	 }
      }
      valU = pow( valU, alpha[u] );
      val += valU;
   }

   return val;
}

double marge( size_t& nEvals, tinyGraph& g, node_id x, vector<bool>& set,
		   vector< bool >& cov = emptySetVector ) {
   if (alpha.size() == 0) {
      init_alpha( g );
   }
   
   if (set[x])
      return 0;

   vector< tinyEdge >& neis = g.adjList[x].neis;
   double gain = 0.0;
   for (size_t j = 0; j < neis.size(); ++j) {
      node_id v = neis[j].target;
      vector< tinyEdge >& neisV = g.adjList[ v ].neis;
      double valV = 0.0;
      double valVwithX = 0.0;
      for (size_t k = 0; k < neisV.size(); ++k) {
	 node_id w = neisV[k].target;
	 if (w != x) {
	    if (set[w]) {
	       valV += neisV[k].weight;
	       valVwithX += neisV[k].weight;
	    }
	 } else {
	    valVwithX += neisV[k].weight;
	 }
      }

      if (valV == 0)
	 gain += pow( valVwithX, alpha[v] );
      else
	 gain += pow( valVwithX, alpha[v] ) - pow( valV, alpha[v] );
   }
   ++nEvals;
   return gain;
}


#endif

void reportResults( size_t nEvals, size_t obj, size_t maxMem = 0 ) {
   allResults.add( "obj", obj );
   allResults.add( "nEvals", nEvals );
   allResults.add( "mem", maxMem );
}


class Frg {
   Args& myArgs;
   size_t k;
   tinyGraph& g;
   size_t nEvals = 0;
   double epsi;
   size_t w;
   size_t W;
public:
   Frg( Args& args ) : myArgs( args ), g( args.g ) {
      k = args.k;
      epsi = args.epsi;
   }

   long leastBenefit( node_id u, vector<bool>& set ) {
      ++nEvals;
      set[u] = false;
      long m = marge( nEvals, g, u, set );
      set[u] = true;
      return m;
   }

   void fillM( vector< node_id >& M, vector< bool >& S ) {

      while (w > epsi*W / k) {
	 for (node_id x = 0; x < g.n; ++x) {
	    if (marge( nEvals, g, x, S ) > (1 - epsi)*w) {
	       M.push_back( x );
	       if ( M.size() >= k )
		  return;
	    }
	 }
	 
	 w = (1 - epsi)*w;
      }
   }

   void run() {
      runRandom();
   }

   void randomSampling(vector<bool>& A ) {
      size_t rho = static_cast<double>(g.n) / k * log( 1 / epsi ) + 1;
      vector< bool > M;
      vector< MyPair > margeGains;
      MyPair tmp;
      vector< bool > cvgA( g.n, false);
      for (size_t i = 0; i < k; ++i) {
	 sampleUnifSize( M, rho );

	 margeGains.clear();
	 for (node_id u = 0; u < g.n; ++u) {
	    if ( M[u] ) {
	       tmp.gain = marge( nEvals, g,u, A, cvgA);
	       tmp.u = u;
	       margeGains.push_back( tmp );
	    }
	 }

	 std::sort( margeGains.begin(), margeGains.end(), revgainLT() );
	 //uniform_int_distribution< size_t > dist(0, s - 1);
	 //size_t rand = dist( gen );
	 node_id u = margeGains[ 0 ].u;
	 A[u] = true;
	 g.coverAdjacent( u, cvgA );
      
      }
   }

   void sampleUnifSize( vector<bool>& R, size_t Size ) {

      if (Size >= g.n - 1) {
	 R.assign(g.n, true);
	 return;
      }
      
      uniform_int_distribution<size_t> dist(0, g.n-1);
      R.assign(g.n, false);

      for (size_t i = 0; i < Size; ++i) {
	 size_t pos;
	 do {
	    pos = dist( gen );
	 } while ( R[ pos ] );
	 R[ pos ] = true;
      }
    
   }

   void runRandom() {
      g.logg << "FastRandom: Running RandomSampling..." << endL;
      vector<bool> S(g.n, false );
      randomSampling( S );
      g.logg << "S: " << compute_valSet( nEvals,  g, S ) << endL;
      g.logg << "Evals: " << nEvals << endL;

      reportResults( nEvals, compute_valSet(nEvals, g, S) );
   }   
   
};

class Rand {
   Args& myArgs;
   size_t k;
   tinyGraph& g;
   size_t nEvals = 0;
   double epsi;

public:
   Rand( Args& args ) : myArgs( args ), g( args.g ) {
      k = args.k;
   }

   void sampleUnifSize( vector<bool>& R, size_t Size ) {

      if (Size >= g.n - 1) {
	 R.assign(g.n, true);
	 return;
      }
      
      uniform_int_distribution<size_t> dist(0, g.n-1);
      R.assign(g.n, false);

      for (size_t i = 0; i < Size; ++i) {
	 size_t pos;
	 do {
	    pos = dist( gen );
	 } while ( R[ pos ] );
	 R[ pos ] = true;
      }
    
   }
   void sampleUnifSize( vector<node_id>& R, size_t Size,
			uniform_int_distribution<size_t>& dist ) {
      for (size_t i = 0; i < Size; ++i) {
	 size_t pos = dist( gen );
	 swap( R[i], R[pos] );
      }
    
   }
   
   void run() {
      double solVal = 0;
      size_t max = g.n;
      vector< node_id > uni;
      uni.reserve(g.n);
      for (node_id u = 0; u < g.n; ++u) {
	 uni.push_back( u );
      }

      vector< node_id > tmp;
      uniform_int_distribution< size_t > dist(0, g.n - 1);
      for (size_t i = 0; i < max; ++i) {

	 sampleUnifSize( uni, k, dist );
	 tmp.assign( uni.begin(), uni.begin() + k );
	 double tmpVal = compute_valSet( nEvals, g, tmp );
	 if (tmpVal > solVal)
	    solVal = tmpVal;
      }
      g.logg << "Random solution value: " << solVal << endL;
      reportResults( nEvals, solVal );
   }
   
};


//Standard Greedy
class Sg {
   size_t k;
   tinyGraph& g;
   size_t nEvals = 0;
public:
   Sg( Args& args ) : g( args.g ) {
      k = args.k;
   }
   
   void run() {
      vector<bool> A( g.n, false );
      vector<bool> cvgA( g.n, false );
      
      double maxGain;
      node_id maxIdx;
      MyPair tmp;

      for (size_t i = 0; i < k; ++i) {
	 maxGain = 0;
	 for (node_id u = 0; u < g.n; ++u) {
	    
	    if (marge( nEvals, g,u,A, cvgA) > maxGain) {
	       maxIdx = u;
	       maxGain = marge( nEvals, g,u,A, cvgA);
	    }
	 }

	 if (maxGain > 0) {
	    A[maxIdx] = true;
	    g.coverAdjacent( maxIdx, cvgA );
	 } else {
	    break;
	 }
      }

      g.logg << "Evals: " << nEvals << endL;
      g.logg << "A: " << compute_valSet( nEvals,  g, A ) << endL;

      reportResults( nEvals, compute_valSet(nEvals, g, A) );
   }
};

//QuickStream
class Qs {
public:
   Args& args;
   size_t k;
   tinyGraph& g;
   size_t nEvals = 0;
   size_t c = 1;
   bool plusplus;
   double solVal = 0.0;
   double epsi;
   double delta;
   size_t ell;
   size_t maxMem = 0;
   
   Qs( Args& inargs ) : args(inargs), g( args.g ) {
      k = inargs.k;
      c = inargs.c;
      plusplus = inargs.plusplus;
      epsi = inargs.epsi;
      delta = inargs.delta;
      //      if (c > 1)
      //	 plusplus = false;
      ell = log( 1 / (4.0*epsi) ) / log( 2 ) + 4;
   }

   double run_BR( vector< bool >& U, // restricted universe
		  double Gamma, //preliminary value
		  double alpha ) { //preliminary ratio
      double tau = Gamma / (alpha * k);

      vector< bool > A( g.n, false );
      vector< bool > cvgA( g.n, false );
      double gamma = tau;
      double valA = 0;
      size_t sizeA = 0;
      size_t maxGain;
      size_t passes = 0;

      vector< double > lastGain( g.n, tau );
      while (tau >= Gamma / (8.0 * k)) {
	 ++passes;
	 tau = tau * ( 1 - epsi );
	 for (node_id u = 0; u < g.n; ++u) {
	    if (U[u]) {
	       if (tau <= lastGain[u]) {
		  double gain = marge(nEvals, g, u, A, cvgA);
		  lastGain[u] = gain;
		  if (gain >= tau) {
		     A[u] = true;
		     g.coverAdjacent( u, cvgA );
		     valA += gain;
	       
		     ++sizeA;
		  }
		  if (sizeA == k)
		     break;
	       }
	    }
	 }
	 if (sizeA == k)
	    break;
      }
      

      g.logg << "Passes: " << passes << endL;
      size_t tmp = 0;
      double solVal = compute_valSet( tmp, g, A );

      return solVal;
   }

#ifndef REVMAX
   void run( bool report = true ) {
      if (c == 1) {
	 runcone( report );
	 return;
      }
      
      vector< node_id > idsA;

      double valA = 0.0;
      vector< size_t > C;
      deque<size_t> Aprime;
      vector<bool> cvgA( g.n, false );

      size_t maxSize = 2.0*c*ell*(k+1)*log( static_cast<double>(k) ) / log(2) + 1;
      g.logg << "maxSize = " << maxSize << endL;
	 
      for (node_id u = 0; u < g.n; ++u) {
	 C.push_back( u );

	 if ( (C.size() == c) || (u == g.n - 1)) {
	    double gain = 0.0;
	    vector< size_t > nodesCovered;
	    double val = 0;
	    for (size_t i = 0; i < g.n; ++i) {
	       if (cvgA[i])
		  ++val;
	    }

	    for (size_t i = 0; i < C.size(); ++i) {
	       idsA.push_back( C[i ] );
	       gain += g.coverAdjacent( C[i], cvgA, nodesCovered );
	       if (idsA.size() > maxMem)
		  maxMem = idsA.size();
	    }

	    ++nEvals;
	    if (gain >= delta * valA / k) {
	       valA = valA + gain;
	       for (size_t i = 0; i < C.size(); ++i) {
		  Aprime.push_back( C[i] );
		  if (Aprime.size() > c*k)
		     Aprime.pop_front();
	       }

	       if (idsA.size() > maxSize) {
		  g.logg << "Deleting from A..." << endL;
		  idsA.assign( idsA.begin() + maxSize / 2, idsA.end() );
	       }
	    } else {
	       //have to revert the state
	       for (size_t i = 0; i < C.size(); ++i) {
		  idsA.pop_back();
	       }
	       for (size_t i = 0; i < nodesCovered.size(); ++i) {
		  cvgA[ nodesCovered[i] ] = false;
	       }

	    }
	       
	    C.clear();
		    
	 }
      }

      g.logg << "Size of Aprime: " << Aprime.size() << endL;
	 
      vector< bool > sol(g.n, false);
      double valSol = 0.0;
      vector< size_t > tmpIds;
      vector< bool > tmpSol(g.n, false);
      deque<size_t>::iterator it1 = Aprime.begin();
      do {
	 if (tmpIds.size() < k) {
	    tmpIds.push_back( *it1 );
	    tmpSol[ *it1 ] = true;
	    ++it1;

	 } else {
	    double tmpVal = compute_valSet( nEvals, g, tmpSol );
	    if (tmpVal > valSol) {
	       sol = tmpSol;
	       valSol = tmpVal;
	    }
	    tmpSol.assign(g.n, false);
	    tmpIds.clear();
	 }
	    
      } while( it1 != Aprime.end() );

      size_t sizeSol = k;
	 
      if (tmpIds.size() > 0) {
	 double tmpVal = compute_valSet( nEvals, g, tmpSol );
	 if (tmpVal > valSol) {
	    sol = tmpSol;
	    valSol = tmpVal;
	    sizeSol = tmpIds.size();
	 }
	 tmpSol.assign(g.n, false);
	 tmpIds.clear();
      }

      if (plusplus) {
	 double Gamma = valSol;
	 g.logg << "Gamma = " << Gamma << endL;
	 g.logg << "Size of restricted universe = " << idsA.size() << endL;
	 double alpha = 1.0/(1 + delta)/(1 + 1/delta)/c;
	 vector< bool > A(g.n, false);
	 for (size_t i = 0; i < idsA.size(); ++i) {
	    A[idsA[i]] = true;
	 }

	 valSol = run_BR( A, Gamma, alpha );
      }
      
      g.logg << "nEvals: " << nEvals << endL;
      g.logg << "valSol: " << valSol << endL;
      g.logg << "maxMem: " << maxMem << endL;

      if (report)
	 reportResults( nEvals, valSol, maxMem );

      solVal = valSol;
   }
#else
      void run( bool report = true ) {
      if (c == 1) {
	 runcone( report );
	 return;
      }
      
      vector< node_id > idsA;
      size_t sizeA = 0;
      double valA = 0.0;
      vector< size_t > C;
      deque<size_t> Aprime;
      vector<bool> cvgA( g.n, false );

      size_t maxSize = 2.0*c*ell*(k+1)*log( static_cast<double>(k) ) / log(2) + 1;
      g.logg << "maxSize = " << maxSize << endL;
	 
      for (node_id u = 0; u < g.n; ++u) {
	 C.push_back( u );

	 if ( (C.size() == c) || (u == g.n - 1)) {
	    vector< node_id > tmpA = idsA;

	    for (size_t i = 0; i < C.size(); ++i) {
	       tmpA.push_back( C[i ] );
	       if (tmpA.size() > maxMem)
		  maxMem = tmpA.size();
	    }
	       
	    double valTmp = compute_valSet( nEvals, g, tmpA );
	    double gain = valTmp - valA;

	    if (gain >= delta * valA / k) {
	       valA = valTmp;
	       for (size_t i = 0; i < C.size(); ++i) {
		  idsA.push_back( C[i] );
		  Aprime.push_back( C[i] );
		  if (Aprime.size() > c*k)
		     Aprime.pop_front();
	       }

	       if (idsA.size() > maxSize) {
		  g.logg << "Deleting from A..." << endL;
		  idsA.assign( idsA.begin() + maxSize / 2, idsA.end() );
	       }
	    }
	       
	    C.clear();
		    
	 }
      }

      g.logg << "Size of Aprime: " << Aprime.size() << endL;
	 
      vector< bool > sol(g.n, false);
      double valSol = 0.0;
      vector< size_t > tmpIds;
      vector< bool > tmpSol(g.n, false);
      deque<size_t>::iterator it1 = Aprime.begin();
      do {
	 if (tmpIds.size() < k) {
	    tmpIds.push_back( *it1 );
	    tmpSol[ *it1 ] = true;
	    ++it1;

	 } else {
	    double tmpVal = compute_valSet( nEvals, g, tmpSol );
	    if (tmpVal > valSol) {
	       sol = tmpSol;
	       valSol = tmpVal;
	    }
	    tmpSol.assign(g.n, false);
	    tmpIds.clear();
	 }
	    
      } while( it1 != Aprime.end() );

      size_t sizeSol = k;
	 
      if (tmpIds.size() > 0) {
	 double tmpVal = compute_valSet( nEvals, g, tmpSol );
	 if (tmpVal > valSol) {
	    sol = tmpSol;
	    valSol = tmpVal;
	    sizeSol = tmpIds.size();
	 }
	 tmpSol.assign(g.n, false);
	 tmpIds.clear();
      }

      if (plusplus) {
	 double Gamma = valSol;
	 g.logg << "Gamma = " << Gamma << endL;
	 g.logg << "Size of restricted universe = " << idsA.size() << endL;
	 double alpha = 1.0/(1 + delta)/(1 + 1/delta)/c;
	 vector< bool > A(g.n, false);
	 for (size_t i = 0; i < idsA.size(); ++i) {
	    A[idsA[i]] = true;
	 }

	 valSol = run_BR( A, Gamma, alpha );
      }
      
      g.logg << "nEvals: " << nEvals << endL;
      g.logg << "valSol: " << valSol << endL;
      g.logg << "sizeSol: " << sizeSol << endL;

      if (report)
	 reportResults( nEvals, valSol, maxMem );

      solVal = valSol;
   }
#endif

  
   void runcone( bool report = true ) {
      vector<bool> A( g.n, false );
      
      deque<size_t> idsA;
      size_t sizeA = 0;
      
      vector<bool> cvgA( g.n, false );
      deque<size_t> Aprime;
      double gain;
      double valA = 0;
      size_t maxSize = 2.0*ell*(k+1)*log( static_cast<double>(k) ) / log(2) + 1;
      size_t nDeletions = 0;
      g.logg << "MaxSize of A: " << maxSize << endL;      
      for (node_id u = 0; u < g.n; ++u) {
      	 gain = marge( nEvals, g,u,A, cvgA);
	 if (gain >= delta * valA / k) {
	    A[u] = true; 
	    g.coverAdjacent( u, cvgA );
	    valA += gain;
	    
	    ++sizeA;
	    if (sizeA > maxMem)
	       maxMem = sizeA;
	    
	    Aprime.push_back( u );
	    idsA.push_back( u );
	    if (Aprime.size() > k) {
	       Aprime.pop_front();
	    }
	 } 
	 
	 if (sizeA > maxSize) {
	    ++nDeletions;
	    while (idsA.size() > maxSize / 2) {
	       idsA.pop_front();
	    }
	    sizeA = maxSize / 2;
	    A.assign(g.n,false);
	    for (size_t i = 0; i < idsA.size(); ++i) {
	       A[ idsA[i] ] = true;
	    }
	 }
      }
      
      g.logg << "nDeletions: " << nDeletions << endL;

      size_t tmpEvals = 0;
      
      g.logg << "Size of Aprime: " << Aprime.size() << endL;
      
      if( Aprime.size() > k ) {
	 cerr << "ERROR: Aprime is too large..." << endl;
      }

      vector< node_id > vAprime( Aprime.begin(), Aprime.end() );
      double valSol = compute_valSet( tmpEvals,  g, vAprime );
      g.logg << "f(A'): " << valSol  << endL;
      
      if (plusplus) {
	 double Gamma = valSol;
	 g.logg << "Gamma = " << Gamma << endL;
	 g.logg << "Size of restricted universe = " << idsA.size() << endL;
	 double alpha = 1.0/(1 + delta)/(1 + 1/delta)/c;
	 vector< bool > A(g.n, false);
	 for (size_t i = 0; i < idsA.size(); ++i) {
	    A[idsA[i]] = true;
	 }

	 valSol = run_BR( A, Gamma, alpha );
      }

      if (report)
	 reportResults( nEvals, valSol, maxMem );
      
      solVal = valSol;
      g.logg << "nEvals: " << nEvals << endL;
      g.logg << "valSol: " << valSol << endL;
      g.logg << "maxMem: " << maxMem << endL;
   }

   void run_alt() {
      if (c == 1) {
	 runcone();
      } else {
	 vector< node_id > idsA;
	 size_t sizeA = 0;
	 double valA = 0.0;
	 vector< size_t > C;
	 deque<size_t> Aprime;

	 vector< vector< size_t > > topKBlocksAdded; 
	 vector< double > topKGainsAdded; 
	 vector<bool> cvgA( g.n, false );
	 
	 for (node_id u = 0; u < g.n; ++u) {
	    C.push_back( u );

	    if ( (C.size() == c) || (u == g.n - 1)) {
	       vector< node_id > tmpA = idsA;

	       for (size_t i = 0; i < C.size(); ++i) {
		  tmpA.push_back( C[i ] );
	       }
	       
	       double valTmp = compute_valSet( nEvals, g, tmpA );
	       double gain = valTmp - valA;

	       if (gain >= c*valA / k) {
		  valA = valTmp;
		  for (size_t i = 0; i < C.size(); ++i) {
		     idsA.push_back( C[i] );
		     Aprime.push_back( C[i] );
		     if (Aprime.size() > k)
			Aprime.pop_front();
		  }

		  if (plusplus) {
		     if (topKGainsAdded.empty()) {
			topKGainsAdded.insert( topKGainsAdded.begin(), gain );
			topKBlocksAdded.insert( topKBlocksAdded.begin(), C );
		     } else {
			vector< double >::iterator itDoub = topKGainsAdded.begin();
			vector< vector< size_t > >::iterator itNI = topKBlocksAdded.begin();

			size_t pos = 0;
			while (*itDoub > gain) {
			   ++itDoub;
			   ++itNI;
			   ++pos;

			   if (itDoub == topKGainsAdded.end())
			      break;
			   if (pos > k)
			      break;
			}

			if (pos < k) {
			   topKGainsAdded.insert( itDoub, gain );
			   topKBlocksAdded.insert( itNI, C );
			}
		     }
		  }
	       }
	       
	       C.clear();
		    
	    }
	 }

	 
	 if (plusplus) {
	    //Aprime is not the last k, but the best k
	    //Aprime.clear();
	    size_t pos = 0;
	    vector< vector< size_t > >::iterator it = topKBlocksAdded.begin();
	    while (it != topKBlocksAdded.end()) {
	       vector< size_t > C = *it;
	       for (size_t i = 0; i < C.size(); ++i) {
		  Aprime.push_back( C[i] );
		  ++pos;
		  if (pos == k)
		     break;
	       }
	       if (pos == k)
		  break;
	       
	       ++it;
	    }
	 }

	 g.logg << "Size of Aprime: " << Aprime.size() << endL;
	 
	 vector< bool > sol(g.n, false);
	 double valSol = 0.0;
	 vector< size_t > tmpIds;
	 vector< bool > tmpSol(g.n, false);
	 deque<size_t>::iterator it1 = Aprime.begin();
	 do {
	    if (tmpIds.size() < k) {
	       tmpIds.push_back( *it1 );
	       tmpSol[ *it1 ] = true;
	       ++it1;

	    } else {
	       double tmpVal = compute_valSet( nEvals, g, tmpSol );
	       cerr << tmpVal << endl;
	       if (tmpVal > valSol) {
		  sol = tmpSol;
		  valSol = tmpVal;
	       }
	       tmpSol.assign(g.n, false);
	       tmpIds.clear();
	    }
	    
	 } while( it1 != Aprime.end() );

	 size_t sizeSol = k;
	 
	 if (tmpIds.size() > 0) {
	    double tmpVal = compute_valSet( nEvals, g, tmpSol );
	    cerr << tmpVal << endl;
	    if (tmpVal > valSol) {
	       sol = tmpSol;
	       valSol = tmpVal;
	       sizeSol = tmpIds.size();
	    }
	    tmpSol.assign(g.n, false);
	    tmpIds.clear();
	 }

	 g.logg << "nEvals: " << nEvals << endL;
	 g.logg << "valSol: " << valSol << endL;
	 g.logg << "sizeSol: " << sizeSol << endL;
	 reportResults( nEvals, valSol );
      }
   }
};

//Qs + Br
class Br {
   size_t k;
   tinyGraph& g;
   size_t nEvals = 0;
   double epsi;
   Qs qs;
public:
   Br( Args& args ) : g( args.g ), qs( args ) {
      k = args.k;
      epsi = args.epsi;
   }

   double runQuickStream() {
      vector<bool> A( g.n, false );
      deque<size_t> idsA;
      size_t sizeA = 0;
      vector<bool> cvgA( g.n, false );
      deque<size_t> Aprime;
      
      double gain;
      double valA = 0;
      size_t maxSize = 4*(k+1)*log( static_cast<double>(k) ) + 1.0;
      size_t nDeletions = 0;
      g.logg << "MaxSize of A: " << maxSize << endL;      
      for (node_id u = 0; u < g.n; ++u) {
      	 gain = marge( nEvals, g,u,A, cvgA);
	 if (gain >= 1.0 * valA / k) {
	    A[u] = true;

	    g.coverAdjacent( u, cvgA );
	    valA += gain;

	    ++sizeA;
	    Aprime.push_back( u );
	    idsA.push_back( u );
	    if (Aprime.size() > k) {
	       Aprime.pop_front();
	    }
	 }
	 if (sizeA > maxSize) {
	    ++nDeletions;
	    while (idsA.size() > maxSize / 2) {
	       idsA.pop_front();
	    }
	    sizeA = maxSize / 2;
	    A.assign(g.n,false);
	    for (size_t i = 0; i < idsA.size(); ++i) {
	       A[ idsA[i] ] = true;
	    }
	 }
      }
      g.logg << "nDeletions: " << nDeletions << endL;
      g.logg << "Evals: " << nEvals << endL;

      size_t tmpEvals = 0;
      
      vector<bool> AprimeTwo(g.n, false);
      for (size_t i = 0; i < Aprime.size(); ++i) {
	 AprimeTwo[ Aprime[i] ] = true;
      }

      double solVal = compute_valSet( tmpEvals,  g, AprimeTwo );
      g.logg << "f(A): " << compute_valSet( tmpEvals,  g, A ) << endL;
      g.logg << "f(A'): " << solVal << endL;
      g.logg << "size of A': " << Aprime.size() << endL;
      g.logg << "size of A: " << sizeA << endL;

      return solVal;
   }
   
   void run() {
      size_t passes = 0;

      qs.epsi = 0.01;
      qs.run( false );
      double Gamma = qs.solVal;
      nEvals = qs.nEvals;
      ++passes;
      //compute ratio of 
      double alpha;
      if (k > 8.0 * qs.c / exp(1)) { //ratio of QuickStreamLargeK
	 alpha = 1.0 / (1.0 + qs.c + 1.0 / (k*k*k - 1) );
	 alpha = alpha * (1.0 - 1.0 / exp(1) - (2.0 * qs.c) / (k*exp(1)) - qs.c*qs.c / (k*k*exp(1)));
      } else { 
	 alpha = 0.25 - qs.epsi;
	 alpha = alpha / qs.c;
      }

      double tau = Gamma / (alpha * k);

      vector< bool > A( g.n, false );
      vector< bool > cvgA( g.n, false );
      double gamma = tau;
      double valA = 0;
      size_t sizeA = 0;
      size_t maxGain;
      while (tau >= Gamma / (8.0 * k)) {
	 ++passes;
	 maxGain = 0;
	 tau = tau * ( 1 - epsi );
	 for (node_id u = 0; u < g.n; ++u) {
	    double gain = marge(nEvals, g, u, A, cvgA);
	    if (gain >= tau) {
	       A[u] = true;
	       g.coverAdjacent( u, cvgA );
	       valA += gain;
	       
	       ++sizeA;
	    }
	    if (gain > maxGain) {
	       maxGain = gain;
	    }
	    if (sizeA == k)
	       break;
	 }
	 if (sizeA == k)
	    break;

	 if (maxGain < tau)
	    tau = maxGain;
      }

      g.logg << "Passes: " << passes << endL;
      g.logg << "Evals: " << nEvals << endL;
      size_t tmp = 0;
      size_t solVal = compute_valSet( tmp, g, A );
      g.logg << "Solution Value: " << solVal << endL;

      reportResults( nEvals, solVal );
   }

};


struct mypair {
   double gain;
   size_t id;
   size_t idxA;
};

struct mypairLT {
   bool operator() ( const mypair& p1, const mypair& p2 ) {
      return p1.gain > p2.gain;
   }
};

//Chakrabarti & Kale
class Ck {
public:
   size_t k;
   tinyGraph& g;
   size_t nEvals = 0;
   size_t c = 1;
   double solVal = 0.0;
   size_t mem = 0;
   
   Ck( Args& args ) : g( args.g ) {
      k = args.k;
      c = args.c;
   }

   void run( bool report = true ) {
      size_t sizeA = 0;
      vector<bool> cvgA( g.n, false );
      vector< node_id > idsA;
      priority_queue< mypair, vector< mypair >, mypairLT > w;
      for (node_id u = 0; u < g.n; ++u) {
	 double valA = compute_valSet( nEvals, g, idsA );
	 idsA.push_back(u);
	 double valAadd = compute_valSet( nEvals, g, idsA );
	 idsA.pop_back();
	 double gain = valAadd - valA;
	 if (sizeA < k) {
	    mypair tmp;
	    tmp.id = u;
	    tmp.gain = gain;
	    tmp.idxA = sizeA;
	    w.push( tmp );
	    ++sizeA;
	    idsA.push_back( u );
	 } else {
	    mypair tmp = w.top();
	    if (gain >= 2*tmp.gain) {
	       idsA[ tmp.idxA ] = u;
	       w.pop();
	       tmp.id = u;
	       tmp.gain = gain;
	       w.push( tmp );
	    }
	 }
      }
	 
      double valSol = compute_valSet( --nEvals, g, idsA );

      g.logg << "nEvals: " << nEvals << endL;
      g.logg << "valSol: " << valSol << endL;

      if (report)
	 reportResults( nEvals, valSol, sizeA );

      solVal = valSol;
   }

};




//ThresholdGreedy
class Tg {
   size_t k;
   tinyGraph& g;
   size_t nEvals = 0;
   double epsi;
public:
   Tg( Args& args ) : g( args.g ) {
      k = args.k;
      epsi = args.epsi;
   }

   void run() {

      


      vector< bool > A( g.n, false );
      vector< bool > cvgA( g.n, false );
      size_t valA = 0;
      size_t sizeA = 0;

      //Get max singleton
      g.logg << "ThresholdGreedy: Determining max singleton..." << endL;
      double M = 0;
      node_id a0;
      for (size_t x = 0; x < g.n; ++x) {
	 if ( marge( nEvals, g, x, A, cvgA ) > M ) {
	    a0 = x;
	    M = marge( nEvals, g, x, A, cvgA );
	 }
      }

      g.logg << "M= " << M << endL;
      
      double tau = M;
      double maxGain;      
      while (tau >= epsi*M / k) {
	 maxGain = 0;
	 tau = tau * ( 1 - epsi );
	 for (node_id u = 0; u < g.n; ++u) {
	    double gain = marge(nEvals, g, u, A, cvgA);
	    if (gain >= tau) {
	       A[u] = true;
	       g.coverAdjacent( u, cvgA );
	       valA += gain;
	       ++sizeA;
	    }
	    if (gain > maxGain) {
	       maxGain = gain;
	    }
	    if (sizeA == k)
	       break;
	 }
	 if (sizeA == k)
	    break;

	 if (maxGain < tau)
	    tau = maxGain;
      }
      
      g.logg << "Evals: " << nEvals << endL;
      size_t tmp = 0;
      size_t solVal = compute_valSet( tmp, g, A );
      g.logg << "Solution Value: " << solVal << endL;

      if (solVal != valA) {
	 cerr << "ERROR\n";
      }
	 
      reportResults( nEvals, solVal );
   }
};

//Sieve-Streaming++
class Ss {
   size_t k;
   tinyGraph& g;
   size_t nEvals = 0;
   double epsi;
   size_t maxMem = 0;
   
   public:
   Ss( Args& args ) : g( args.g ) {
      k = args.k;
      epsi = args.epsi;
   }

   size_t sizeSet( vector<bool>& S ) {
      size_t ssize = 0;
      for (size_t i = 0; i < g.n; ++i) {
	 if (S[i])
	    ++ssize;
      }
      return ssize;
   }
   
   void run() {
      vector< bool > emptySet( g.n, false );
      double tau_min = 0;
      double Delta = 0;
      double LB = 0;

      deque< double > threshVals;
      deque< vector< bool > > sols;
      deque< vector< bool > > cvgs;
      deque< size_t > sizes;
      deque< double > vals;
      
      for (node_id u = 0; u < g.n; ++u) {
	 double singletonVal = marge(nEvals, g, u, emptySet, emptySet );
	 if (singletonVal > Delta) {
	    Delta = singletonVal;
	 }

	 double maxLD = LB;
	 if (Delta > maxLD)
	    maxLD = Delta;
	 
	 tau_min = maxLD / (2.0*k);

	 if (threshVals.size() > 0) {
	    while ( threshVals[0] < tau_min ) {
	       threshVals.pop_front();
	       sols.pop_front();
	       cvgs.pop_front();
	       maxMem -= sizes[0];
	       sizes.pop_front();
	       vals.pop_front();
	    }
	 } else {
	    threshVals.push_back( tau_min );
	    sols.push_back( emptySet );
	    cvgs.push_back( emptySet );
	    sizes.push_back( 0 );
	    vals.push_back( 0 );
	 }

	 double curr_thresh = tau_min;
	 size_t pos = 0;
	 while (curr_thresh <= Delta) {
	    if (pos >= threshVals.size()) {
	       threshVals.push_back( curr_thresh );
	       sols.push_back( emptySet );
	       cvgs.push_back( emptySet );
	       sizes.push_back( 0 );
	       vals.push_back( 0 );
	    }
	    vector< bool >& curr_sol = sols[ pos ];
	    vector< bool >& curr_cvg = cvgs[ pos ];
	    size_t sizeSol = sizes[ pos ];
	    if (sizeSol < k) {
	       double gain = marge( nEvals, g, u, curr_sol, curr_cvg);
	       if ( gain >= curr_thresh) {
		  curr_sol[u] = true;
		  ++(sizes[pos]);
		  g.coverAdjacent( u, curr_cvg );
		  vals[pos] += gain;
		  ++maxMem;
		  if (vals[pos] > LB) {
		     LB = vals[pos];
		  }
	       }
	    }
	    ++pos;
	    curr_thresh = curr_thresh * (1 + epsi);
	 }
      }

      double valSol = 0;
      size_t tmpst = 0;
      for (size_t i = 0; i < vals.size(); ++i) {
	 if (sizeSet( sols[i] ) > k) {
	    cerr << "ERROR, sol too large\n";
	    return;
	 }
	 if (vals[i] != compute_valSet( nEvals, g, sols[i] )) {
	    //cerr << "ERROR, values incorrect:"
	    //<< vals[i] << ' ' << compute_valSet( tmpst, g, sols[i] ) << endl;
	    vals[i] = compute_valSet( tmpst, g, sols[i] );
	 }
	 if (vals[i] > valSol) {
	    valSol = vals[i];
	 }
      }

      g.logg << "solution value: " << valSol << endL;
      g.logg << "number of queries: " << nEvals << endL;
      g.logg << "memory used: " << maxMem << endL;
      reportResults( nEvals, valSol, maxMem );
   }
};

//Buchbinder, Feldman, Schwartz 2018
class Bs {
   size_t k;
   tinyGraph& g;
   size_t nEvals = 0;
public:
   Bs( Args& args ) : g( args.g ) {
      k = args.k;
   }
   
   void run() {
      vector<bool> A( g.n, false );
      vector<size_t> idsA;
      size_t sizeA = 0;
      vector<bool> cvgA( g.n, false );
      deque<size_t> Aprime;
      
      size_t valA = 0;
      
      for (node_id u = 0; u < g.n; ++u) {
	 if (sizeA < k) {
	    A[u] = true;
	    valA += g.coverAdjacent( u, cvgA );
	    ++sizeA;
	 } else {
	    size_t maxIdx = 0;
	    size_t maxVal = 0;
	    size_t idx = 0;
	    for (size_t i = 0; i < k; ++i) {
	       while ( A[idx] == false )
		  ++idx;
	       A[u] = true;
	       A[idx] = false;
	       size_t tmpVal = compute_valSet( nEvals, g, A );
	       if (tmpVal > maxVal) {
		  maxIdx = idx;
		  maxVal = tmpVal;
	       }
	       A[u] = false;
	       A[idx++] = true;
	    }
	    if (maxVal - valA >= valA / k) {
	       A[u] = true;
	       A[maxIdx] = false;
	       valA = maxVal;
	    }
	 }
      }

      g.logg << "Evals: " << nEvals << endL;

      size_t tmpEvals = 0;
      
      g.logg << "f(A): " << compute_valSet( tmpEvals,  g, A ) << endL;
      g.logg << "size of A: " << sizeA << endL;

      reportResults( nEvals, compute_valSet(tmpEvals, g, A) );
   }
};


//P-Pass
class Pp {
   size_t k;
   tinyGraph& g;
   size_t nEvals = 0;
   double epsi;
   size_t P;
   double tradeoff;
   
   public:
   Pp( Args& args ) : g( args.g ) {
      k = args.k;
      tradeoff = args.tradeoff;
      epsi = tradeoff*args.epsi;
      P = 1;//args.P;
      double factor = (double)P / (P + 1);
      double bound = 1/exp(1) + args.epsi*(1 - tradeoff);
      //bound = 1.0 / bound;
      bound = log( bound );
      //cerr << bound << endl;
      //cerr << factor << endl;
      while ( P * log( factor) > bound ) {
	 //cerr << P * log( factor ) << endl;
	 ++P;
	 factor = (double)P / (P + 1);
      }

      g.logg << "P-Pass initialized, P=" << P << endL;
   }

   size_t sizeSet( vector<bool>& S ) {
      size_t ssize = 0;
      for (size_t i = 0; i < g.n; ++i) {
	 if (S[i])
	    ++ssize;
      }
      return ssize;
   }
   
   void run() {
      vector< bool > emptySet( g.n, false );
      double tau_min = 0;
      double Delta = 0;

      deque< double > threshVals; //(1,0.0);
      deque< vector< bool > > sols;// (1, vector<bool>());
      deque< vector< bool > > cvgs;// (1, vector<bool>());
      deque< size_t > sizes; // (1,0);
      deque< double > vals; // (1, 0.0);

      double factor = static_cast<double>( P ) / (P + 1);
      double T = factor;
      g.logg << "P/(P+1) = " << factor << endL;
      //Do the first pass
      for (node_id u = 0; u < g.n; ++u) {
	 double singletonVal = marge(nEvals, g, u, emptySet, emptySet );
	 if (singletonVal > Delta) {
	    Delta = singletonVal;
	 }

	 double maxLD = Delta;
	 
	 tau_min = maxLD;

	 if (threshVals.size() > 0) {
	    while ( threshVals[0] < tau_min ) {
	       threshVals.pop_front();
	       sols.pop_front();
	       cvgs.pop_front();
	       sizes.pop_front();
	       vals.pop_front();
	    }
	 } else {
	    threshVals.push_back( tau_min );
	    sols.push_back( emptySet );
	    cvgs.push_back( emptySet );
	    sizes.push_back( 0 );
	    vals.push_back( 0 );
	 }

	 double curr_thresh = tau_min;
	 size_t pos = 0;
	 while (curr_thresh <= Delta * k / T) {
	    if (pos >= threshVals.size()) {
	       threshVals.push_back( curr_thresh );
	       sols.push_back( emptySet );
	       cvgs.push_back( emptySet );
	       sizes.push_back( 0 );
	       vals.push_back( 0 );
	    }
	    vector< bool >& curr_sol = sols[ pos ];
	    vector< bool >& curr_cvg = cvgs[ pos ];
	    size_t sizeSol = sizes[ pos ];
	    if (sizeSol < k) {
	       double gain = marge( nEvals, g, u, curr_sol, curr_cvg);
	       if ( gain >= curr_thresh * T / k) {
		  curr_sol[u] = true;
		  ++(sizes[pos]);
		  g.coverAdjacent( u, curr_cvg );
		  vals[pos] += gain;
	       }
	    }
	    ++pos;
	    curr_thresh = curr_thresh * (1 + epsi);
	 }
      }

      //Do remaining P - 1 passes
      for (size_t i = 0; i < P - 1; ++i) {
	 T = T * factor;
	 for (node_id u = 0; u < g.n; ++u) {
	    double curr_thresh = threshVals[0];
	    size_t pos = 0;
	       while (curr_thresh <= Delta * k / T) {
		  if (pos >= threshVals.size()) {
		     threshVals.push_back( curr_thresh );
		     sols.push_back( emptySet );
		     cvgs.push_back( emptySet );
		     sizes.push_back( 0 );
		     vals.push_back( 0 );
		  }

		  vector< bool >& curr_sol = sols[ pos ];
		  vector< bool >& curr_cvg = cvgs[ pos ];
		  size_t sizeSol = sizes[ pos ];
		  if (sizeSol < k) {
		     double gain = marge( nEvals, g, u, curr_sol, curr_cvg);
		     if ( gain >= curr_thresh * T / k) {
			curr_sol[u] = true;
			++(sizes[pos]);
			g.coverAdjacent( u, curr_cvg );
			vals[pos] += gain;
		     }
		  }
		  curr_thresh = curr_thresh * (1 + epsi);
		  ++pos;
	       }
	 }
      }

      
      double valSol = 0;
      for (size_t i = 0; i < vals.size(); ++i) {
	 size_t tmpst = 0;
	 vals[i] = compute_valSet( tmpst, g, sols[i] );
	 if (vals[i] > valSol) {
	    valSol = vals[i];
	 }
      }

      g.logg << "solution value: " << valSol << endL;
      g.logg << "number of queries: " << nEvals << endL;

      reportResults( nEvals, valSol );
   }
};

#endif
